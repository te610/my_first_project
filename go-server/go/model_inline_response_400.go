/*
 * paysim
 *
 * testing
 *
 * API version: 1.0.2
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger

type InlineResponse400 struct {
	// error message
	Msg string `json:"msg,omitempty"`
}
